import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/blocs/scale.dart';
import 'package:gdg_slides_2018/src/common/keyboard.dart';
import 'package:gdg_slides_2018/src/navigation/transitions/fade_transition.dart';
import 'package:gdg_slides_2018/src/navigation/transitions/no_transition.dart';
import 'package:gdg_slides_2018/src/navigation/transitions/slide_and_fade_transition.dart';
import 'package:gdg_slides_2018/src/providers/scale.dart';
import 'package:gdg_slides_2018/src/views/dart_logo.dart';
import 'package:gdg_slides_2018/src/views/dart_feature.dart';
import 'package:gdg_slides_2018/src/views/dart_target.dart';
import 'package:gdg_slides_2018/src/views/flutter_demo.dart';
import 'package:gdg_slides_2018/src/views/flutter_embedder.dart';
import 'package:gdg_slides_2018/src/views/flutter_hummingbird.dart';
import 'package:gdg_slides_2018/src/views/flutter_hummingbird_detail.dart';
import 'package:gdg_slides_2018/src/views/flutter_is.dart';
import 'package:gdg_slides_2018/src/views/flutter_logo.dart';
import 'package:gdg_slides_2018/src/views/flutter_platform.dart';
import 'package:gdg_slides_2018/src/views/flutter_system.dart';
import 'package:gdg_slides_2018/src/views/flutter_widget.dart';
import 'package:gdg_slides_2018/src/views/home.dart';
import 'package:gdg_slides_2018/src/views/me.dart';
import 'package:gdg_slides_2018/src/views/thanks.dart';

class GdgSlides extends StatefulWidget {
  @override
  _GdgSlidesState createState() => _GdgSlidesState();
}

class _GdgSlidesState extends State<GdgSlides> {
  final routesName = [
    HomeView.routeName,
    MeView.routeName,
    DartLogoView.routeName,
    DartFeatureView.routeName,
    DartTargetView.routeName,
    FlutterLogoView.routeName,
    FlutterDemoView.routeName,
    FlutterIsView.routeName,
    FlutterSystemView.routeName,
    FlutterWidgetView.routeName,
    FlutterPlatformView.routeName,
    FlutterEmbedderView.routeName,
    FlutterHummingbirdView.routeName,
    FlutterHummingbirdDetailsView.routeName,
    ThanksView.routeName,
  ];

  ScaleBloc scaleBloc;

  @override
  void initState() {
    super.initState();
    scaleBloc = ScaleBloc();
  }

  @override
  void dispose() {
    scaleBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaleProvider(
      scaleBloc: scaleBloc,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'GDG Toulouse - Flutter',
        color: Colors.blue,
        onGenerateRoute: _onGenerateRoute,
      ),
    );
  }

  Route _onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case HomeView.routeName:
        return _generateFadeRoute(
          routeSettings,
          HomeView.routeName,
          HomeView(),
        );
      case MeView.routeName:
        return _generateSlideAndFadeRoute(
          routeSettings,
          MeView.routeName,
          MeView(),
        );

      //  Dart
      case DartLogoView.routeName:
        return _generateNoTransitionRoute(
          routeSettings,
          DartLogoView.routeName,
          DartLogoView(),
        );
      case DartFeatureView.routeName:
        return _generateFadeRoute(
          routeSettings,
          DartFeatureView.routeName,
          DartFeatureView(),
        );
      case DartTargetView.routeName:
        return _generateFadeRoute(
          routeSettings,
          DartTargetView.routeName,
          DartTargetView(),
        );

      //  Flutter
      case FlutterLogoView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterLogoView.routeName,
          FlutterLogoView(),
        );
      case FlutterDemoView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterDemoView.routeName,
          FlutterDemoView(),
        );
      case FlutterIsView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterIsView.routeName,
          FlutterIsView(),
        );
      case FlutterSystemView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterSystemView.routeName,
          FlutterSystemView(),
        );
      case FlutterWidgetView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterWidgetView.routeName,
          FlutterWidgetView(),
        );
      case FlutterPlatformView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterPlatformView.routeName,
          FlutterPlatformView(),
        );
      case FlutterEmbedderView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterEmbedderView.routeName,
          FlutterEmbedderView(),
        );
      case FlutterHummingbirdView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterHummingbirdView.routeName,
          FlutterHummingbirdView(),
        );
      case FlutterHummingbirdDetailsView.routeName:
        return _generateFadeRoute(
          routeSettings,
          FlutterHummingbirdDetailsView.routeName,
          FlutterHummingbirdDetailsView(),
        );

      //  other
      case ThanksView.routeName:
        return _generateFadeRoute(
          routeSettings,
          ThanksView.routeName,
          ThanksView(),
        );
    }
    return null;
  }

  Route _generateFadeRoute(
      RouteSettings routeSettings, String routeName, Widget child) {
    return FadeTransitionRoute(
      builder: (_) => KeyboardNavigation(
            currentRoute: routeName,
            routesName: routesName,
            child: child,
          ),
      settings: routeSettings,
    );
  }

  Route _generateSlideAndFadeRoute(
      RouteSettings routeSettings, String routeName, Widget child) {
    return SlideAndFadeTransitionRoute(
      builder: (_) => KeyboardNavigation(
            currentRoute: routeName,
            routesName: routesName,
            child: child,
          ),
    );
  }

  Route _generateNoTransitionRoute(
      RouteSettings routeSettings, String routeName, Widget child) {
    return NoTransitionRoute(
      builder: (_) => KeyboardNavigation(
            currentRoute: routeName,
            routesName: routesName,
            child: child,
          ),
    );
  }
}
