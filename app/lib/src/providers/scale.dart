import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/blocs/scale.dart';

class ScaleProvider extends InheritedWidget {
  static ScaleProvider of(BuildContext context) =>
      context.ancestorWidgetOfExactType(ScaleProvider);

  final ScaleBloc scaleBloc;

  ScaleProvider({@required Widget child, this.scaleBloc}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
