import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterSystemView extends StatelessWidget {
  static const routeName = '/flutter_system';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Image.asset(
          'assets/flutter_system_overview.png',
        ),
      );
    });
  }
}
