import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterDemoView extends StatelessWidget {
  static const routeName = '/flutter_demo';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Container(
          width: 350 * scaleFactor,
          height: 500 * scaleFactor,
          child: MyHomePage(
            title: 'Flutter Demo Home Page',
            scaleFactor: scaleFactor,
          ),
        ),
      );
    });
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.scaleFactor}) : super(key: key);

  final String title;
  final double scaleFactor;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(widget.title),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
              style: TextStyle(fontSize: 12 * widget.scaleFactor),
            ),
            Text(
              '$_counter',
              style: TextStyle(
                fontSize: 10 * widget.scaleFactor,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
