import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterHummingbirdDetailsView extends StatelessWidget {
  static const routeName = '/flutter_hummingbird_details';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Image.asset(
          'assets/hummingbird_details.png',
        ),
      );
    });
  }
}
