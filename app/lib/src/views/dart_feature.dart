import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class DartFeatureView extends StatelessWidget {
  static const routeName = '/dart_simple_syntax';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(
      builder: (context, scaleFactor) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Divider(
                color: Colors.blue,
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 7 * scaleFactor),
                child: Text(
                  'General purpose language',
                  style: TextStyle(
                    fontSize: 12 * scaleFactor,
                  ),
                ),
              ),
              Divider(
                color: Colors.blue,
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 7 * scaleFactor),
                child: Text(
                  'Strong static type',
                  style: TextStyle(
                    fontSize: 12 * scaleFactor,
                  ),
                ),
              ),
              Divider(
                color: Colors.blue,
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 7 * scaleFactor),
                child: Text(
                  'OOP',
                  style: TextStyle(
                    fontSize: 12 * scaleFactor,
                  ),
                ),
              ),
              Divider(
                color: Colors.blue,
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 7 * scaleFactor),
                child: Text(
                  'Async/Await',
                  style: TextStyle(
                    fontSize: 12 * scaleFactor,
                  ),
                ),
              ),
              Divider(
                color: Colors.blue,
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 7 * scaleFactor),
                child: Text(
                  'Full featured SDK',
                  style: TextStyle(
                    fontSize: 12 * scaleFactor,
                  ),
                ),
              ),
              Divider(
                color: Colors.blue,
                height: 1,
              ),
            ],
          ),
    );
  }
}
