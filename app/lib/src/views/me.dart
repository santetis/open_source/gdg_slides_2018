import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/common/flutter_logo.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';
import 'package:gdg_slides_2018/src/global/keys.dart';

class MeView extends StatelessWidget {
  static const routeName = '/me';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(
      builder: (context, scaleFactor) {
        return Stack(
          children: [
            Positioned(
              width: 200,
              height: 200,
              bottom: -40,
              right: -10,
              child: Hero(
                tag: flutterHeroGlobalTag,
                child: FlutterLogoWidget(),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 100 * scaleFactor,
                    child: ClipOval(
                      child: Container(
                        child: Image.asset('assets/profile.jpg'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0 * scaleFactor),
                    child: Text(
                      'Kevin Segaud',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12 * scaleFactor,
                      ),
                    ),
                  ),
                  Text(
                    'GDE Dart/Flutter',
                    style: TextStyle(
                        color: Colors.grey, fontSize: 10 * scaleFactor),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/twitter_logo.png',
                        width: 13 * scaleFactor,
                      ),
                      Text(
                        '@kleak_dev',
                        style: TextStyle(
                          fontSize: 8 * scaleFactor,
                          color: Color(0xFF1DA1F2),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
