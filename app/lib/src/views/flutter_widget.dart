import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterWidgetView extends StatelessWidget {
  static const routeName = '/flutter_widget';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'Material',
                      style: TextStyle(fontSize: 12 * scaleFactor),
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: RaisedButton(
                            child: Text('Button'),
                            onPressed: () {},
                          ),
                        ),
                        CircularProgressIndicator(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: AppBar(
                            elevation: 0,
                            title: Text('AppBar'),
                            automaticallyImplyLeading: false,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            VerticalDivider(),
            Expanded(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Text(
                      'Cupertino',
                      style: TextStyle(fontSize: 12 * scaleFactor),
                    ),
                  ),
                  Divider(),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: CupertinoButton(
                            child: Text(
                              'Button',
                              style: TextStyle(fontFamily: 'Roboto'),
                            ),
                            onPressed: () {},
                          ),
                        ),
                        CupertinoActivityIndicator(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: CupertinoNavigationBar(
                            automaticallyImplyLeading: false,
                            middle: Text(
                              'NavigationBar',
                              style: TextStyle(fontFamily: 'Roboto'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
