import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterIsView extends StatelessWidget {
  static const routeName = '/flutter_i';

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ScaleWidget(
      builder: (context, scaleFactor) => Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    color: Colors.blue,
                    width: size.width / 2,
                    height: size.height / 2,
                    child: Center(
                      child: Text(
                        'Open source',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12 * scaleFactor,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.green,
                    width: size.width / 2,
                    height: size.height / 2,
                    child: Center(
                      child: Text(
                        'Beautiful',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12 * scaleFactor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    color: Colors.orange,
                    width: size.width / 2,
                    height: size.height / 2,
                    child: Center(
                      child: Text(
                        'Fast',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12 * scaleFactor,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.purple,
                    width: size.width / 2,
                    height: size.height / 2,
                    child: Center(
                      child: Text(
                        'Productive',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12 * scaleFactor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
    );
  }
}
