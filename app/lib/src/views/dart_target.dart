import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class DartTargetView extends StatelessWidget {
  static const routeName = '/dart_target';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(
      builder: (context, scaleFactor) => Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/picto_serveurs.png',
                      width: 70 * scaleFactor,
                    ),
                    Text(
                      'Dart VM',
                      style: TextStyle(fontSize: 12 * scaleFactor),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(32.0 * scaleFactor),
                  child: Container(),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/logo_javascript.png',
                      width: 70 * scaleFactor,
                    ),
                    Text('Javascript',
                        style: TextStyle(fontSize: 12 * scaleFactor)),
                  ],
                ),
              ],
            ),
          ),
    );
  }
}
