import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterPlatformView extends StatelessWidget {
  static const routeName = '/flutter_platform';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Image.asset(
          'assets/platform_channel.png',
        ),
      );
    });
  }
}
