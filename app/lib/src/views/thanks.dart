import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class ThanksView extends StatelessWidget {
  static const routeName = '/thanks';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Container(
        color: Color(0xff00517C),
        child: Center(
          child: Text(
            'Merci !',
            style: TextStyle(fontSize: 16 * scaleFactor, color: Colors.white),
          ),
        ),
      );
    });
  }
}
