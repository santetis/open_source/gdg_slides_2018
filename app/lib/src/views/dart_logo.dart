import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class DartLogoView extends StatelessWidget {
  static const routeName = '/dart_logo';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(
      builder: (context, scaleFactor) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 100 * scaleFactor,
                  child: Image.asset('assets/dart_logo.png'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15 * scaleFactor),
                  child: Text(
                    'Dartlang',
                    style: TextStyle(fontSize: 13 * scaleFactor),
                  ),
                ),
              ],
            ),
          ),
    );
  }
}
