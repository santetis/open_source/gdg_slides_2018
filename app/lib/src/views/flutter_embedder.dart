import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';

class FlutterEmbedderView extends StatelessWidget {
  static const routeName = '/flutter_embedder';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(builder: (context, scaleFactor) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Image.asset(
                  'assets/android_logo.png',
                  width: 50 * scaleFactor,
                ),
                Image.asset(
                  'assets/ios_logo.jpeg',
                  width: 50 * scaleFactor,
                ),
                Image.asset(
                  'assets/chrome_os.png',
                  width: 50 * scaleFactor,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Image.asset(
                  'assets/linux_logo.png',
                  width: 50 * scaleFactor,
                ),
                Image.asset(
                  'assets/mac_logo.png',
                  width: 50 * scaleFactor,
                ),
                Image.asset(
                  'assets/windows_logo.png',
                  width: 50 * scaleFactor,
                ),
              ],
            ),
          ],
        ),
      );
    });
  }
}
