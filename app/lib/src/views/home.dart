import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gdg_slides_2018/src/common/flutter_logo.dart';
import 'package:gdg_slides_2018/src/common/scale.dart';
import 'package:gdg_slides_2018/src/global/keys.dart';

class HomeView extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return ScaleWidget(
      builder: (context, scaleFactor) => Scaffold(
            body: Center(
              child: SizedBox(
                width: 180 * scaleFactor,
                height: 135 * scaleFactor,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      width: 100 * scaleFactor,
                      child: Hero(
                        tag: flutterHeroGlobalTag,
                        child: FlutterLogoWidget(),
                      ),
                    ),
                    Positioned(
                      top: 20 * scaleFactor,
                      left: 70 * scaleFactor,
                      child: Transform.rotate(
                        angle: (-45.0 * (2 * pi)) / 360,
                        child: Text(
                          'lutter',
                          style: TextStyle(
                            fontSize: 44 * scaleFactor,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
    );
  }
}
