import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/providers/scale.dart';

typedef Widget ScaleBuilder(BuildContext context, double scaleFactor);

class ScaleWidget extends StatelessWidget {
  final ScaleBuilder builder;

  ScaleWidget({@required this.builder});

  @override
  Widget build(BuildContext context) {
    final scaleBloc = ScaleProvider.of(context).scaleBloc;
    return StreamBuilder(
      initialData: scaleBloc.scaleFactor.value,
      stream: scaleBloc.scaleFactor,
      builder: (context, snapshot) => Scaffold(
            body: Container(
              child: builder(context, snapshot.data),
            ),
          ),
    );
  }
}
