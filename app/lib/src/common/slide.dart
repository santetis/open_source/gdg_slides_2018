import 'package:flutter/widgets.dart';

class SlideWidget extends StatefulWidget {
  SlideWidget({this.child, this.next, this.previous});

  final void Function(BuildContext context) previous;
  final void Function(BuildContext context) next;
  final Widget child;

  @override
  _SlideWidgetState createState() => _SlideWidgetState();
}

class _SlideWidgetState extends State<SlideWidget> {
  Offset delta;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: _onUpdateDrag,
      onHorizontalDragEnd: _endDrag,
      child: widget.child,
    );
  }

  void _onUpdateDrag(DragUpdateDetails details) {
    delta = details.delta;
  }

  void _endDrag(DragEndDetails details) {
    if (delta.dx > 0) {
      widget.previous(context);
    } else {
      widget.next(context);
    }
  }
}
