import 'package:flutter/widgets.dart';

class FlutterLogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/flutter_logo.png',
    );
  }
}
