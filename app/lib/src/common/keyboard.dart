import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gdg_slides_2018/src/common/slide.dart';
import 'package:gdg_slides_2018/src/providers/scale.dart';

class KeyboardNavigation extends StatelessWidget {
  final _textNode = new FocusNode();
  final Widget child;
  final List<String> routesName;
  final String currentRoute;

  KeyboardNavigation({this.child, this.routesName, this.currentRoute});

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).requestFocus(_textNode);
    final scaleBloc = ScaleProvider.of(context).scaleBloc;
    return SlideWidget(
      previous: _previousPage,
      next: _nextPage,
      child: RawKeyboardListener(
        focusNode: _textNode,
        onKey: (key) {
          if (key is RawKeyUpEvent) {
            final data = key.data;
            if (data is RawKeyEventDataAndroid) {
              if (data.keyCode == 262) {
                _nextPage(context);
              } else if (data.keyCode == 263) {
                _previousPage(context);
              } else if (data.keyCode == 334) {
                scaleBloc.increaseScale.add(null);
              } else if (data.keyCode == 333) {
                scaleBloc.decreaseScale.add(null);
              }
            }
          }
        },
        child: child,
      ),
    );
  }

  void _nextPage(BuildContext context) {
    final index = routesName.indexOf(currentRoute);
    if (index + 1 < routesName.length) {
      Navigator.of(context).pushNamed(routesName[index + 1]);
    }
  }

  void _previousPage(BuildContext context) {
    final index = routesName.indexOf(currentRoute);
    if (index - 1 >= 0) {
      Navigator.of(context).pop();
    }
  }
}
