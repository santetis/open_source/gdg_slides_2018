import 'package:rxdart/rxdart.dart';

class ScaleBloc {
  BehaviorSubject<double> _scaleBehavior;
  final _increaseScaleFactor = PublishSubject<void>();
  final _decreaseScaleFactor = PublishSubject<void>();

  ScaleBloc({double scaleFactor: 1}) {
    _scaleBehavior = BehaviorSubject(seedValue: scaleFactor);

    _increaseScaleFactor.stream
        .listen((_) => _scaleBehavior.add(_scaleBehavior.value + 1));
    _decreaseScaleFactor.stream
        .listen((_) => _scaleBehavior.add(_scaleBehavior.value - 1));
  }

  Sink<void> get increaseScale => _increaseScaleFactor.sink;
  Sink<void> get decreaseScale => _decreaseScaleFactor.sink;

  ValueObservable<double> get scaleFactor => _scaleBehavior.stream;

  void dispose() {
    _increaseScaleFactor.close();
    _decreaseScaleFactor.close();
  }
}
