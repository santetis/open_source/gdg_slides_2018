import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gdg_slides_2018/src/app.dart';

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(GdgSlides());
}
